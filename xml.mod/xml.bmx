
SuperStrict

Module MinGW.XML

Import Text.XML

'Import BRL.Base64
'Import PUB.zLib

Public

Rem
Type TTiledNode Extends TNode
	
	Public
	
	Method FromString:TNode( str:String )
		Local node:TNode = Super.FromString( str )
		ProcessNode( node )
		Return node
	End Method
	
	Private
	
	Function ProcessNode( node:TNode )
		Local data:Byte[]
		
		If node["tag"]="data"
			If node["encoding"]="base64"
				
				data=TBase64.Decode( node["content"] )
				
				If node["compression"]="zlib"
				
					Local length:UInt=65536
					Local buffer:Byte[ length ]
						
					Local error:Int=uncompress( buffer,length,data,UInt( data.Length ) )
					If error<>0
					
					End If
					data=buffer[..length]
					
				Else If node["compression"]="gzip"
					RuntimeError("gzip no support!")
				End If
				
				node["content"]=String.FromBytes( data,data.Length )
			End If
		End If
		
		For Local child:TNode=EachIn node
			ProcessNode( child )
		Next
	End Function
End Type
End Rem

Type xNode
	
	Function Load:xNode( url:Object )
		Return New xDoc( TxmlDoc.ParseFile( url ) )
	End Function
	
	Method Operator[]:String( key:String )
		key=key.ToLower()
		
		Select key
		Case "tag" 		Return _node.getName()
		Case "content" 	Return _node.getContent()
		Default			Return _node.getAttribute( key )
		End Select
		
		Return ""
	End Method
	
	Method Find:xNode( tag:String,deep:Int=0 )
		Return Find( tag,"","",deep )
	End Method
	
	Method Find:xNode( tag:String,key:String,value:String,deep:Int=0 )
		'If deep=0
			For Local child:xNode=EachIn Self
				If tag<>""
					If key<>""
						If child["tag"]=tag And child[key]=value Then Return child
					Else
						If child["tag"]=tag Then Return child
					End If
				Else
					If key<>""
						If child[key]=value Then Return child
					Else
						Return child
					End If
				End If
				
				If deep=0 Then Continue
				
				child=child.Find( tag,key,value,deep-1 )
				If child Then Return child
				
			Next
			Return Null
			
		'End If
		'Local n:TxmlNode=_node.findElement( tag,key,value )
		'If n
		'	Local i:INode=New INode
		'	i._node=n
		'	Return i
		'End If
	End Method
	
	Method FindAll:xNode[]( tag:String,deep:Int=0 )
		Return FindAll( tag,"","",deep )
	End Method
	
	Method FindAll:xNode[]( tag:String,key:String,value:String,deep:Int=0 )
		all=New xNode[0]
		fall( tag,key,value,deep )
		Return all
	End Method
	
	Method ObjectEnumerator:TListEnum()
		Local list:TList=New TList
		For Local n:TxmlNode=EachIn _node.getChildren()
			'Local i:INode=New INode
			'i._node=n
			list.AddLast( New xNode( n ) )
		Next
		Return list.ObjectEnumerator()
	End Method
	
	Private
	
	Global all:xNode[0]
	
	Method fall( tag:String,key:String,value:String,deep:Int )
		
		
		For Local child:xNode=EachIn Self
			If tag<>""
				If key<>""
					If child["tag"]=tag And child[key]=value Then all:+[child]
				Else
					If child["tag"]=tag Then all:+[child]
				End If
			Else
				If key<>""
					If child[key]=value Then all:+[child]
				Else
					all:+[child]
				End If
			End If
			
			If deep=False Then Continue
			
			child.fall( tag,key,value,deep-1 )
			
		Next
		
		
	End Method
	
	Method New( n:TxmlNode )
		_node=n
	End Method
	
	Field _node:TxmlNode
End Type

Private

Type xDoc Extends xNode
	
	Method New( doc:TxmlDoc )
		_doc=doc
		_node=doc.getRootElement()
	End Method
	
	Method Delete()
		_doc.Free()
		_doc=Null
		_node=Null
	End Method

	Field _doc:TxmlDoc
End Type

Rem
Type XNode

	Private
	
	Const TAG_OPEN:Int=Asc( "<" )
	Const TAG_CLOSE:Int=Asc( "/" )
	Const TAG_END:Int=Asc( ">" )
	
	Global lastNode:XNode, tabs:String
	
	Field keys:String[]=["UnknownTag"]
	Field vals:String[]=[""]
	
	Field childs:XNode[0]
	'Field countChilds:Int
	
	Method _ParseTag( str:String )
		
		Local char:Int,inQuotes:Int,lastWasSpace:Int=True,spaces:Int=0,on:String,lasta:String[]
		For Local i:Int=0 Until str.Length
			char=str[i]
			
			If Not inQuotes And (_isWhiteSpace( char ) Or (i=str.Length-1))
				
				If Not lastWasSpace
					If Not spaces
						If i=str.Length-1 Then on:+Chr(char)
						Self["tag"]=on
						on=""
						spaces=True
					End If
					lastWasSpace=True
				End If
				
			Else If Not inQuotes And char=61 '=
				
				lasta = [on,""]
				on=""
				lastWasSpace=False
				
			Else If char=34
			
				If inQuotes
					Assert lasta, "Encountered malformed tag."
					lasta[1] = on
					on = ""
					Self[lasta[0].ToLower()]=lasta[1]
					lasta = Null
				End If
				inQuotes=Not inQuotes
				lastWasSpace=False
			
			Else
				on:+Chr( char )
				lastWasSpace=False
			End If
		Next
	End Method
	
	Function _isWhiteSpace:Int( char:Int )
		Return char = 32 Or char = 9 Or char = 10 Or char = 13 ' space,tab,newline
	End Function
	
	Global start:Int
	
	Method _FromString:XNode( str:String )
		
		Local char:Int,content:String,tag:String,opened:Byte
		For Local i:Int=start Until str.Length
			char=str[i]
			
			Select char
			Case TAG_OPEN '<
			
				content=content.Trim()
				If content<>""
					Assert lastNode,"Encountered illegal parent node."
					lastNode["content"]=content
				End If
				opened=True
				Continue
				
			Case TAG_END
			
				Assert tag.Length,"Encountered illegal tag: <>"
				start=i+1
				'DebugLog "NEW TAG:"+tag
				Exit
				
			End Select
			
			If opened
				tag:+Chr( char )
			Else
				content:+Chr( char )
			End If
		Next
		
		If tag.StartsWith( "?" ) And tag.EndsWith( "?" )
			Assert tag.ToLower().Contains( "version=~q1.0~q" ),		""
			Assert tag.ToLower().Contains( "encoding=~qutf-8~q" ),	""
			Return _FromString( str )
		End If
		
		If tag.StartsWith( "/" ) 'its a closing tag
			Return Null
		Else If tag.EndsWith( "/" ) 'has no childs
			tag=tag[..tag.Length-1]
		Else 'has childs
			lastNode=Self
			Repeat
				'DebugLog str
				Local child:XNode=New XNode._FromString( str )
				If Not child Then Exit
				AddChild( child )
			Forever
			lastNode=Null
		End If
		_ParseTag( tag )
		
		Return Self
	End Method
	
	Public
	
	Method New()
	End Method
	
	Method New( tag:String )
		Self["tag"]=tag
	End Method
	
	Method New( parent:XNode )
		parent.AddChild( Self )
	End Method
	
	Method New( tag:String,parent:XNode )
		Self["tag"]=tag
		parent.AddChild( Self )
	End Method
	
	Method New( parent:XNode,tag:String )
		Self["tag"]=tag
		parent.AddChild( Self )
	End Method
	
	Method FromString:XNode( str:String,debug:Int=False )
		Self.debug=debug
		start=0
		Return _FromString( str )
	End Method
	
	Method ToString:String()
		
		Local line:String=""
		If tabs="" Then line="<?xml version=~q1.0~q encoding=~qUTF-8~q?>~r~n"
		
		line:+tabs+"<"+keys[0] 'Self["tag"]
		
		Local i:Int
		For i=1 Until keys.Length
			'If keys[i].StartsWith("tag") Then Continue
			'If keys[i].StartsWith("content") Then Continue
			line:+" "+keys[i]+"=~q"+vals[i]+"~q"
		Next
		
		Local content:String=vals[0] 'Self["content"]
		
		If childs.Length=0 And content=""
			line:+"/>~r~n"
		Else
			Local oldTabs:String=tabs
			tabs:+"~t"
			line:+">~r~n"
			For i=0 Until childs.Length
				line:+childs[i].ToString()
			Next
			If content Then line:+tabs+content+"~r~n"
			tabs=oldTabs
			line:+tabs+"</"+keys[0]+">~r~n"
		End If
		
		Return line
	End Method
	
	Method operator[]:String( key:String )
		key=key.ToLower()
		
		Select key
		Case "tag" 		Return keys[0]
		Case "content" 	Return vals[0]
		Default
			For Local i:Int=1 Until keys.Length
				If keys[i]=key Then Return vals[i]
			Next
		End Select
		
		Return ""
	End Method
	
	Method operator[]:String( index:Int )
		Return keys[index+1]
	End Method
	
	Method operator[]=( key:String,value:String )
		key=key.ToLower()
		
		Select key
		Case "tag"
			If value<>"" Then keys[0]=value
			Return
		Case "content"
			vals[0]=value
			Return
		Default
			For Local i:Int=1 Until keys.Length
				If keys[i]<>key Then Continue
				If value=Null
					keys=keys[..i]+keys[i+1..]
					vals=vals[..i]+vals[i+1..]
				Else
					vals[i]=value
				End If
				Return
			Next
		End Select
		
		If key="" Then Return
		If value="" Then Return
		
		keys:+[key] 'add
		vals:+[value]
	End Method
	
	Method CountAttribs:Int()
		Return keys.Length-1
	End Method
	
	Field debug:Int=0
	
	Method AddChild( child:XNode )
		For Local i:Int=0 Until childs.Length
			If childs[i]=child Then Return
		Next
		childs:+[child]
		If debug
		DebugLog child["tag"] + ":added"
		DebugLog "COUNT:"+childs.Length
		End If
	End Method
	
	Method RemChild( child:XNode )
		For Local i:Int=0 Until childs.Length
			If childs[i]=child
				childs=childs[..i]+childs[i+1..]
				Return
			End If
		Next
	End Method
	
	Method Find:XNode( tag:String="",key:String="",value:String="",deep:Int=0 )
		For Local i:Int=0 Until childs.Length
			If tag<>""
				If childs[i].keys[0]=tag '["tag"]=tag
					If key<>""
						If childs[i][key]=value Then Return childs[i]
					Else
						Return childs[i]
					End If
				End If
			Else If key<>""
				If childs[i][key]=value Then Return childs[i]
			End If
			
			If Not deep Then Continue
			If deep>0 Then deep:-1
			
			Local child:XNode=childs[i].Find( tag,key,value,deep )
			If child Then Return child
		Next
	End Method
	
	Private 
	
	Global res:XNode[]
	
	Method _FindAll( tag$,key$,value$,deep% )
		For Local i:Int=0 Until childs.Length
			
			If tag<>""
				If childs[i].keys[0]=tag
					If childs[i][key]=value Then res:+[childs[i]] 'Return childs[i]
				End If
			Else If key<>""
				If childs[i][key]=value Then res:+[childs[i]] 'Return childs[i]
			End If
			
			If Not deep Then Continue
			childs[i]._FindAll( tag,key,value,deep-1 )
		Next
	End Method
	
	Public
	
	Method FindAll:XNode[]( tag:String="",key:String="",value:String="",deep:Int=0 )
		res=New XNode[0]
		If deep<0 Then deep=1000
		_FindAll( tag,key,value,deep )
		Return res
	End Method
	
	Method ObjectEnumerator:TArrayEnum()
		Return New TArrayEnum( childs )
	End Method
End Type

Type TArrayEnum 'Extends TEnum
	Field array:Object[]
	Field pos% = 0
	
	Method New( arr:Object[] )
		array = arr
	End Method
	
	Method HasNext:Int()
		Return pos < array.Length
	End Method
	
	Method NextObject:Object()
		pos:+1
		Return array[pos - 1]
	End Method
End Type
end rem